/** @file eppltadd.c
Platform specific additional command functions go here
*/
/**
Copyright 1979-2024 Mark Giebler

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Author: Mark Giebler

 */
#include "epdbcnfg.h"	/* this file is where you configure options for the debug monitor */
#include "eppltfrm.h"	/* this file is where you configure platform specific data types */

#include "epdebug.h"

#include "eppltadd.h"	/* this file is where you define platform specific command additions and functions */

#ifdef EP_CONFIG_USE_PLATFORM_ADDITIONS

/* Define to include platform specific function and command additions */



#ifndef NBAUD
/******************************
baud [48|96|19|38|57] bits [7|8] parity [E|O|N]

used to change serial port parameters
rates supported: 48(00), 96(00), 19(200), 38(400), 57(600), 11(5200).

******************************/
void baud(void)
{
	uint32_t rate, bits;
	char parity;
	Parity p;
	NumBits n;
	char btail;

	// set defaults values:
	rate = 0x38;
	bits = 8;
	parity = 'n';

	pushBack = 0;
	putStr("aud [__]\b\b\b");
	getNumber(&rate,2);

	btail = 0;
	switch(rate)
		{
		case 0x48:
		rate = 4800L;
		break;
		case 0x96:
		rate = 9600L;
		break;
		/*
		case 0x14:
		rate = 14400L;
		btail = '4';
		break;
		 */
		case 0x19:
		rate = 19200L;
		btail = '2';
		break;
		/*
		case 0x28:
		rate = 28800L;
		break;
		 */
		case 0x38:
		rate = 38400L;
		btail = '4';
		break;
		case 0x57:
		rate = 57600L;
		btail = '6';
		break;
		case 0x11:
		rate = 115200L;
		putStr("52");
		break;

		default:
		putStr("\r\nBad rate: valid 48,96,19,38,57\r\n");
		return;
		}
	if(btail)
		put(btail);

	putStr("00] bits[_]");
	repChar('\b',2);
	getNumber(&bits,1);

	// bits check
	if( bits == 7)
		n = BITS_7;
	else if( bits == 8)
		n = BITS_8;
	else {
		putStr("\r\nBad bits: valid 7,8\r\n");
		return;
	}

	putStr("] parity [_]");
	repChar('\b',2);
	parity = get();
	put(parity);
	if( parity == ' ' )
		parity = 'n';
	parity |= 0x60; // force to lower case.

	// parity check
	if( parity == 'e')
		p = EVEN_PARITY;
	else if( parity == 'o')
		p = ODD_PARITY;
	else if( parity == 'n')
		p = NO_PARITY;
	else {
		putStr("\r\nBad parity: valid:e,o,n\r\n");
		return;
	}
	putStr("\r\nChanging port settings...\r\n");
	int32_t timeout = 18104L;
		while( --timeout != 0L)
		; // delay 1 second (.5 second at 16 MHz)
	setPortParameters( rate, n, p, ONE_STOP, False);	/* replace with platform specific method */
}

#endif /* NBAUD */


#ifndef NPFLASH
/****************************************
program flash memory menu
 ****************************************/
#include <burn.h>
void pflash(void)
{
	putStr("rogram Flash."CRLF);
	burn(); /* burn handles download and burn process..*/
#ifndef NSWHNDSHK
	put( XON );
#endif
}
#endif

/* ****************************************************** */
#endif /* end of #ifdef EP_CONFIG_USE_PLATFORM_ADDITIONS  */
/* ****************************************************** */
