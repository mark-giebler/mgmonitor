/** @file epdebug.c
Copyright 1979-2024 Mark Giebler

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Author: Mark Giebler

 */
/*************/
#include "epdbcnfg.h"	/* this file is where you configure options for the debug monitor */
#include "eppltfrm.h"	/* this file is where you configure platform specific data types */

#include "epdebug.h"

#ifdef EP_CONFIG_USE_PLATFORM_ADDITIONS
#include "eppltadd.h"	/* this file is where you define platform specific command additions and functions */
//#include "eppltadd.c"	/* this file is where you define platform specific command additions and functions */
#endif

#define VERSION	" v1.3.1"


/* Version history at the end of file */

/*******************************************************

	Debug monitor code.
Copyright (c) 1979-2024.
By Mark J. Giebler
email:  mjgiebler@yahoo.com

This C code is based on a debug monitor originally written for the
8080/Z80 in assembler by Mark Giebler and Chris Elmquist in 1979 when
we were the leading edge high school geeks.

The basic functions were rewritten in C to make the
debug monitor portable to other CPU's.

How this works:

Entering commands:
commands are prompted for by "cmd: ".
the command is entered followed by a space or <cr>
The last command performed can be repeated at the "cmd: " prompt
by entering just a <cr> or space.
If <cr> is used, then the command is re-executed with default values
and the address parameter is incremented to the next address.
This comes in handy with the DUMP command.
If space is used, then the command is re-executed but parameters are
prompted for.

Entering values:
When a command needs a value it will output something like: [____]
This indicates that up to 4 hex value are needed.
If just a space or <cr> is entered, a default value will be used.
The default value will be displayed inside the [].
If fewer than 4 hex values are entered, the digits entered will be right justified.
I.e. in the above example, if just 'A' was entered the value used would be 000A.
If a <cr> is entered, any remaining parameters for a command are set to
defaults along with the current one.
If a space is entered, the next value needed by the command would be
prompted for.

********************************************************/

/* *******************************************************
*  Private Macros
**********************************************************/


#define _STRINGIFY(name)  #name
#define STRINGIFY(a) _STRINGIFY(a)

#ifndef NSWHNDSHK
#define XOFF 0x13	/* Ctrl-S */
#define XON  0x11	/* Ctrl-Q */
#endif

#ifndef _EP_ADDR
#define _EP_ADDR 6	/* target CPU's number of hex digits per address value */
#endif

/* *******************************************************
*  Private Prototypes
**********************************************************/
void putDumpHeader(void);

/*****************************************************************


replace the following macros in epplatform.h
with target CPU/hardware specific code:

	_EP__PUT()		// required - put a char out serial link.
	_EP__GETCH()	// required - non-blocking, get a char from input, else return -1.
	_EP__GET(),		// (optional) blocking,  wait for and get a char
	_EP__MCU_RESET(),	// (optional) -  reset the MCU
	_EP__RX_WAITING(),	// (optional) -  check if char available, but don't remove from input buffer.

 *****************************************************************/

/**********************************
* globals used by debug code *
**********************************/
char pushBack = { 0 };	 /* used to push one character back to the input for dump() */
uintptr_t dmpAddr = { 0 };	/* dump address */
uintptr_t memAddr = { 0 };;	/* modify memory address */
uintptr_t callAddr;			/* call addr */


/**************************************************************
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	epdebug:
	beginning of target independent debug routines and globals.

	The function debugMonitor() is the entry point, call it
	from your external code.

 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 **************************************************************/
void debugInit(void){
	_EP__INIT();	// call any platform specific initialization to get IO up.
}
#ifndef NRESET
/*****************************

	reset()

******************************/
static void reset()  /* reset the target (if possible) */
{
	_EP__PUTSTR(" system" CRLF );
	_EP__MCU_RESET();	/* this function will not return if the target was actually reset */
}
#endif
/* ********************************
 * Input methods
********************************* */
#ifndef NINPUT
#ifndef _EP__RX_WAITING
#define _EP__RX_WAITING() rxWaiting() /* return same as _EP__GETCH() */
/*********************************
*
*	_EP__RX_WAITING()
*	rxWaiting()
*
* check if character waiting in uart, but don't clear it.
*  This is only used to check for an XOFF rx'ed before put()
*
*  Return -1 if no character received,
* otherwise return the character received, but don't clear it.
***********************************/
int16_t rxWaiting(void)
{
	int16_t c;
	c = _EP__GETCH();
	if(c != -1)
		pushBack = c;	/* save the char for rxChar() */
	return c;
}
#endif
/*******************************
 *
 *  _EP__GET()
 *  rxGet()
 *
 *  If a _EP__GET() is not defined, this function will be used.
 *  Get a character from the input device.
 *  Blocks until a character is available.
 *******************************/
#ifndef _EP__GET
#define _EP__GET() rxGet()
int16_t rxGet(void)
{
	int16_t c;
	do{
		c = _EP__GETCH();
	}while(c == -1);
	return c;

}
#endif

/*******************************

	rxChar()
Handles Xon/Xoff.
Does not block if no char available.
Blocks if Xoff received until Xon received.
return -1 if no char.
*******************************/

int16_t rxChar(){
	int16_t c;
	if(pushBack)
	{
		c = pushBack;
		pushBack = 0;
	}else{
		c = _EP__GETCH();
	}
#ifndef NSWHNDSHK
	if(c == XOFF)	/* Xoff, so wait for Xon */
	{
		while( c != XON) /* wait for Xon */
		{
			c = _EP__GET(); /* blocking call to get char */
		}
	}
	if(c == XON)
		c = -1;
#endif
	return c;
}
#endif
/**************************************

	put()

output one character
handles xon/xoff flow control
always allows XON to go out to prevent dead lock.
***************************************/
void put(char c)
{
#ifndef NSWHNDSHK
	if( c != XON && _EP__RX_WAITING() == XOFF) /* if xoff received */
		rxChar();	 /* get the xoff and wait for xon */
#endif
	_EP__PUT(c);

}
/*******************************************
* putStr
* output null terminated string.
*
********************************************/
#ifndef _EP__PUTSTR
#define _EP__PUTSTR(s) putStr(s)
void putStr(const char* str)
{
	while(*str)
		put(*str++);
}
#endif

/***************
strcmp from K&R C book.  Needed since VxWorks not have strcmp().
NULL means string match.
****************/
int16_t strcmp__(const char* s1, const char* s2)
{
	int16_t result;
/* loop while strings are equal or until NULL */
	for(result = 0; (result = *s1 - *s2) == 0 ; s1++, s2++){
		if (*s1 == 0)
			return 0;
		if (*(s1+1) == '\007')
			return 0;	/* substring match.*/
	}
	return result;
}


/* my contact info */
#define _A_ @
#define _DT_ .
#define _EMAIL "mark.giebler"STRINGIFY(_A_)"gmail"STRINGIFY(_DT_)"com"

/* define return codes for the getNumber routine */
#undef OK__
#define OK__ 0
#define SP 0x20
#define CR 0x0d
#define LF 0x0a
#define BS 0x08
#define TB 0x09


#define RPT 0x21	/* repeat last value '!' */
#define NEXT TB
#define PREV BS

#define SIGN_ON "Debug Monitor" VERSION " by "_EMAIL

#ifndef NABOUT
const char aboutStr[] =
"Copyright (c) 1979-2024." CRLF \
"By "_EMAIL CRLF \
"or http://mjgiebler.freeservers.com/ " CRLF \
"or http://www.geocities.com/mjgiebler/ " CRLF \
CRLF \
"This C code is based on a debug monitor originally written" CRLF \
"in 1979 for the 8080 CPU" CRLF \
"when I was one of the few leading edge high school geeks." CRLF \
"I used this on an 8080 computer I built in high school." CRLF \
"The basic functions were written in C to make the" CRLF \
"debug monitor portable to other CPU's." CRLF \
CRLF \
"How this works:" CRLF \
CRLF \
"Entering commands:" CRLF \
"Commands are prompted for by cmd: ." CRLF \
"The command character is followed by a space or <cr>" CRLF \
"The last command performed can be repeated at the cmd:  prompt" CRLF \
"by entering just a <cr> or space." CRLF \
"If <cr> is used, then the command is re-executed with default values" CRLF \
"and the address parameter is incremented to the next address." CRLF \
"This comes in handy with the d (DUMP) command." CRLF \
"If space is used, then the command is re-executed but parameter" CRLF \
"values are prompted for." CRLF \
CRLF \
"Entering parameter values:" CRLF \
"When a command needs a value it will output something like: [____]" CRLF \
"This indicates that up to 4 hex digits are needed.  If just a" CRLF \
"space or <cr> is entered, a default value will be used (the default value" CRLF \
"will be displayed inside the []).  If fewer than" CRLF \
"4 hex values are entered, the digits entered will be right justified." CRLF \
"I.e. in the above example, if just 'A' was entered the value used" CRLF \
"would be 000A." CRLF \
"If a <cr> is entered, any remaining parameters for a command are set to" CRLF \
"defaults along with the current one." CRLF \
"If a space is entered, the next value needed by the command would be" CRLF \
"prompted for." CRLF;
#endif

/*******************************************
* output routines.
*
********************************************/
/*******************************************
* repChar
* output character the requested number of times.
*
********************************************/
void repChar(char c, int16_t reps)
{
	while(reps){
		put(c);
		reps--;
	}
}
/*********************************************
 * putHex
 *
 * digits is the number of hex digits,
 * e.g. to output 1 byte, digits must be 2.
 * if only one digit output, LSB is used.
 *******************************************/
void putHex(uintptr_num_t number, uint8_t digits)
{
	uint8_t hex;
	do{
		digits--;
		hex = (uint8_t)((number >> (digits * 4)) & 0x0f);
		hex += 0x30;
		if(hex > 0x39)
			hex += 7;
		put((char) hex);
	}while(digits);
}
void putByte(uint8_t byte)
{
	putHex((uintptr_num_t) byte, 2);
}
void putWord(uint16_t word)
{
	putHex((uintptr_num_t) word, 4);
}
void putAddr(uintptr_t addr)
{
	putHex((uintptr_num_t)addr, _EP_ADDR);
}
/**************************************
INPUT routines
 **************************************/
#ifndef NINPUT
/*********************************************/
/* wait for one character to be  input
* filter out linefeeds so terminals that send cr and lf
* when enter key is pressed still work.      */
/*********************************************/
char get()
{
	int16_t c;
	if(pushBack)
	{
		c = pushBack;
		pushBack = 0;
		return c;
	}
	c = -1;
	do{
		c = rxChar();
	}while(c == -1 || c == EOL_IGNORE);  /* loop while NULL or ignore character */
	return (char)c;
}
/***********************************
get an entire line or until max chars found.
*************************************/
char* gets__( char* buf, int16_t max )
{
	int16_t i = 0;
	char c = 0;
	while( i < max && c != 0x0d )
	{
		c = get();
		buf[i] = c;
		i++;
	}
	buf[i] = 0;
	return buf;
}


/**********************************
* This methods gets the request number of hex digits from serial port.
* It fills *number with the digits input.
* On entry *number must have the default value which will be output if
* no valid digit is entered.
*
* Returns:
* 	0    OK__  if it got at least one hex digit.
* 	If no number entered returns or If exit early: Pushback variable will have:
*	0x20 SP if it got a space
*	0x0d if it got return
*	0x08 if it got backspace
*	0x0a if it got linefeed
* Exit:
*	*number is only modified if at least one hex digits is input
*	if fewer that the requested digits is input, pushBack is set
*	to the input char that terminated digit input early.
*************************************/

int16_t getNumber(uint32_t* number, int16_t digits)
{
	int16_t hex;
	int16_t digitsRemaining = digits;

	char c;
	if((c = get()) < '0'){
		/* if no number entered, echo default number and return char that caused exit. */
		putHex(*number,digits);
		return c;
	}
	*number = 0;
	while(digitsRemaining){
		if(c > 0x29){
			/* make room for new digit */
			*number = *number << 4;
#ifndef NECHO
			put(c); /* echo input */
#endif
			/* convert to hex */
			hex = c - 0x30;
			if(hex > 9){
				hex |= 0x20;
				hex -= 0x27;
			}
			*number |= (hex & 0x0f);
			digitsRemaining--;
		}
		/* get next digit if we need it */
		if(digitsRemaining && (c = get()) <= ' '){
			if(c == '\b'){
#ifndef NECHO
				/* if local echo, we need to take care of rubbing out the deleted character */
				_EP__PUTSTR("\b \b");
#endif
				*number = *number >> 4; /* remove last */
				digitsRemaining++;
			}else{ /* input terminated early */
				/* erase user input so we can echo the number with leading 0's */
				repChar('\b',digits - digitsRemaining);
#ifdef NECHO
				/* if no local echo, then remote side is echoing, so need to get rid of space */
				if( c == SP )
					put(BS);
#endif
				/* echo number with leading 0's */
				putHex(*number,digits);
				digitsRemaining = 0;
				/* save the char that caused early exit, SP, CR, LF, or ESC, etc */
				pushBack = c;
			}
		}
	}
	return OK__;
}

int16_t getByte(uint8_t* byte)
{
	int16_t rtn;
	uint32_t nr;
	nr = *byte;
	rtn = getNumber(&nr, 2);
	if(rtn == 0)
		*byte = (uint8_t) nr;
	return rtn;
}

int16_t getWord(uint16_t *word)
{
	int16_t rtn;
	uint32_t nr;
	nr = *word;
	rtn = getNumber(&nr, 4);
	if(rtn == 0)
		*word = (uint16_t) nr;
	return rtn;
}

int16_t getAddr(uintptr_t* addr)
{
	return getNumber((uint32_t*)addr, _EP_ADDR);
}
#endif
/****************************************************
 *         Name: get2
 *  Description:
 *      Decodes 2 ASCII hex characters from buf & returns corresponding data byte
 *
 *  Arguments:
 *      (1) buf  (r/w)  Pointer to data to decode
 *
 *  Returns:
 *      The integer value corresponding to 2 ASCII characters, except
 *      return -1 if either character does not correspond to a hex digit
 *
 */
int16_t get2(char *buf)
{
	int16_t num;
	int16_t chr;

	/*
	 * Decode the 1st character
	 */
	chr = buf[0];
	if (chr >= '0' && chr <= '9')
		num = (chr - '0') << 4;
	else if (chr >= 'A' && chr <= 'F')
		num = (chr - ('A' - 10)) << 4;
	else if (chr >= 'a' && chr <= 'f')
		num = (chr - ('a' - 10)) << 4;
	else
		return (-1);

	/*
	 * Decode the 2nd character and add to first
	 */
	chr = buf[1];
	if (chr >= '0' && chr <= '9')
		num += (chr - '0');
	else if (chr >= 'A' && chr <= 'F')
		num += (chr - ('A' - 10));
	else if (chr >= 'a' && chr <= 'f')
		num += (chr - ('a' - 10));
	else
		return (-1);

	return (num);
}

/****************************************
****************************************
*
* base routines
*
****************************************
*****************************************/
#ifdef NFANCYHDR
/** The original dump header from 1979 */
#define DMP_HDR " 0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F"
#else
/** Define spacer to position second line of header after the address space */
#if _EP_ADDR == 8
#define DMP_SPACE "         "
#else
#define DMP_SPACE "     "
#endif
/** The fancier dump header */
#define DMP_HDR		"00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F  <................>" CRLF \
		DMP_SPACE	"-----------------------------------------------  ------------------"
#endif
void putDumpHeader(void){
	_EP__PUTSTR(CRLF); repChar(' ',_EP_ADDR+1); _EP__PUTSTR(DMP_HDR CRLF);
}

/****************
dump memory in bytes inclusively between start and end.
maximum of 16 bytes.
In absoluteAlignment mode,
pad start and end of line with spaces to aligned heading to location in memory.
In relativeAlignment mode first memory location is aligned with 0 in DMP_HDR heading line.

*****************/
uintptr_t dmpLine(uintptr_t start, uintptr_t end, ep_dumpMode_t dmode)
{
	uintptr_t addrs;
	uint8_t val;
	int16_t i;

	addrs = start;
	if(dmode == absoluteAlignment ){
		addrs = addrs >> 4; /* aligned start to multiple of 16 */
		addrs = addrs << 4;
	}
	putAddr(addrs);
	put(' ');
	i = 16;
	while(i){	/* output 16 addresses */
		if(addrs < start || addrs > end){	/* pad with spaces */
			_EP__PUTSTR("  ");
		}else{
			val = *((uint8_t*)addrs);
			putByte(val);
		}

		put(' ');
		addrs++;

		i--;
	}
	/* output ASCII for the dump. */
	addrs =  start;
	if(dmode == absoluteAlignment ){
		addrs = addrs >> 4; /* aligned start to multiple of 16 */
		addrs = addrs << 4;
	}
	put(' ');
	put('<');
	i = 16;
	while(i){	/* output 16 addresses */
		if(addrs < start || addrs > end){	/* pad with spaces */
			_EP__PUTSTR(" ");
		}else{
			val = *((uint8_t*)addrs);
			if(val < ' ' || val > '~')
				val = '.';		/* unprintable */
			put(val);
		}
		addrs++;
		i--;
	}
	_EP__PUTSTR(">" CRLF);
	if(end > addrs)
		return(addrs);	/* return next address to dump */
	return end+1;  /* return next possible dump address */
}
/***************************************
 * dumpMemory()
 * hex dump memory block with out user input.
 * start - first memory location to dump
 * end   - last location to dump
 * dmode - In absoluteAlignment mode,  pad start and end of line with spaces to aligned heading to location in memory.
 *       - In relativeAlignment mode first memory location is aligned with 0 in DMP_HDR heading line.
 * returns the next address after last dumped address.
****************************************/
uintptr_t dumpMemory(uintptr_t start, uintptr_t end, ep_dumpMode_t dmode){
	putDumpHeader();
	while(start < end ){ /* hex/ascii dump of memory 16 bytes at a time */
		start = dmpLine( start,  end-1, dmode);
	}
	return start;
}

#ifndef NDUMP
/***************************************
* dump command
* Prompt for start and end addresses
* dump at least 16 bytes and wait for input
*	CR will exit, any other char will dump next 16 bytes.
*
* to dump data without prompting for UI input:
*	set pushBack to <CR>
*	set dmpAddr
*
****************************************/
void dump()
{
	int16_t result;
	int16_t c;
	uintptr_t endAddr;

	endAddr = (dmpAddr >> 4);	/* set default dump end addr */
	endAddr = endAddr << 4;		/* set default to dump */
	endAddr = endAddr | 0x0f;	/* one line */

	if(pushBack != CR && pushBack != RPT){
		/* cr or repeat where not used to start this command
		then get new values. */

		/* get starting address */

		pushBack = 0;

		_EP__PUTSTR("ump [");
		repChar('_',_EP_ADDR);
		_EP__PUTSTR("]");
		repChar('\b',_EP_ADDR+1);
		result = getAddr(&dmpAddr);

		/* update end addr */
		endAddr = (dmpAddr >> 4);	/* set default dump end addr */
		endAddr = endAddr << 4;		/* set default to dump */
		endAddr = endAddr | 0x0f;	/* one line */

		/* get end address to dump */
		if(result == OK__ || result == SP){
			pushBack = 0;
			_EP__PUTSTR("] to [");
			repChar('_',_EP_ADDR);
			put(']');
			repChar('\b',_EP_ADDR+1);
			result = getNumber((void*)&endAddr,_EP_ADDR);
		}

		putDumpHeader();
	}
	pushBack = 0;
	//put(CR);

	c = 0;
	/* now dump the requested address range */
	/* until all dumped or <LF> (or <CR>) entered */
	while(dmpAddr <= endAddr){
		dmpAddr = dmpLine(dmpAddr, endAddr, absoluteAlignment);

		/* check if user wants to exit or pause dump */
		if((c = rxChar()) != -1){
			if(c == ABORT_CHAR){ /* LF or CR terminates the dump. */
				endAddr = dmpAddr+1;
				break;
			}else{	/* any other key pauses dump */
				_EP__PUTSTR("dump: PAUSED  <CR> to exit, any other key to continue:\r");
				c = 0;
				while((c = rxChar()) == -1)
					; /* wait for char to resume */
				if(c == ABORT_CHAR){
					endAddr = dmpAddr+1;
					break;
				}
			}
		}
	}
	repChar(' ',60);	/* remove any pause string */
	_EP__PUTSTR(CRLF);
}
#endif

#ifndef NMEMBYTE
/********************************
modify memory with byte values.
 ********************************/
void modifyMemByte()
{
	uint8_t val = 0;
	uint8_t oldVal;
	int16_t result;

	if(pushBack != ABORT_CHAR && pushBack != RPT)
	{
		pushBack = 0;
		_EP__PUTSTR("odify [");
		repChar('_',_EP_ADDR);
		put(']');
		repChar('\b',_EP_ADDR+1);
		getAddr(&memAddr);
		_EP__PUTSTR(CRLF);
	}
	/*
	* display location and wait for byte to overwrite.
	*/
	pushBack = 0;
	put(CR);
	result = OK__;
	while(result != ABORT_CHAR){
		putAddr(memAddr);
		oldVal = *(uint8_t*)memAddr;
		put(' ');
		putByte(oldVal);
		_EP__PUTSTR(" __\b\b");
		result = getByte(&val);
		if(result == OK__ || result == RPT)
		{
			*(uint8_t*) memAddr = val;
			pushBack = 0;
		}else
			_EP__PUTSTR("\b\b  ");
		if(result == PREV)
			memAddr -= 1;
		else
			memAddr += 1;
		_EP__PUTSTR(CRLF);
	}
}
#endif

#ifndef NMEMWORD
/********************************
modify memory with word uint16_t values.
 ********************************/
void modifyMemWord()
{
	uint16_t val = 0;
	uint16_t oldval;
	int16_t result;

	if(pushBack != ABORT_CHAR && pushBack != RPT)
	{
		pushBack = 0;
		_EP__PUTSTR("odify [");
		repChar('_',_EP_ADDR);
		put(']');
		repChar('\b',_EP_ADDR+1);
		getAddr(&memAddr);
		_EP__PUTSTR(CRLF);
	}
	/*
	* display location and wait for word to overwrite.
	*/
	pushBack = 0;
	put(CR);
	result = OK__;
	while(result != ABORT_CHAR){
		putAddr(memAddr);
		oldval = *(uint16_t*)memAddr;
		put(' ');
		putWord(oldval);
		_EP__PUTSTR(" ____\b\b\b\b");
		result = getWord(&val);
		if(result == OK__ || result == RPT)
		{ /* overwrite with new or previous value */
			*(uint16_t*) memAddr = val;
			pushBack = 0;
		}else /* erase echo of default that getNumber() did */
			_EP__PUTSTR("\b\b\b\b    ");
		if(result == PREV)
			memAddr -= 2;
		else
			memAddr += 2;
		_EP__PUTSTR(CRLF);
	}
}
#endif

#ifndef NMEMLONG
/********************************
modify memory with long (int32_t) values.
 ********************************/
void modifyMemLong()
{
	uint32_t val = 0;
	uint32_t oldval;
	int16_t result;

	if(pushBack != ABORT_CHAR && pushBack != RPT)
	{
		pushBack = 0;
		_EP__PUTSTR("odify [");
		repChar('_',_EP_ADDR);
		put(']');
		repChar('\b',_EP_ADDR+1);
		getAddr(&memAddr);
		_EP__PUTSTR(CRLF);
	}
	/*
	* display location and wait for word to overwrite.
	*/
	pushBack = 0;
	put(CR);
	result = OK__;
	while(result != ABORT_CHAR){
		putAddr(memAddr);
		oldval = *(uint32_t*)memAddr;
		put(' ');
		putHex(oldval,8);
		_EP__PUTSTR(" ________\b\b\b\b\b\b\b\b");
		result = getNumber(&val,8);
		if(result == OK__ || result == RPT)
		{ /* overwrite with new or previous value */
			*(uint32_t*) memAddr = val;
			pushBack = 0;
		}else /* erase echo of default that getNumber() did */
			_EP__PUTSTR("\b\b\b\b\b\b\b\b        ");
		if(result == PREV)
			memAddr -= 4;
		else
			memAddr += 4;
		_EP__PUTSTR(CRLF);
	}
}
#endif

#ifndef NFILL
/*****************************************
fill memory.
fill [start] to [end] with [byte]
 ****************************************/
void fill()
{
	int16_t result = OK__;
	uintptr_t start = 0;
	uintptr_t end = 0;
	uint8_t val = 0;
	_EP__PUTSTR("ill [");

	/* get start address */
	pushBack = 0;
	repChar('_',_EP_ADDR);
	put(']');
	repChar('\b',_EP_ADDR+1);
	result = getNumber(&start,_EP_ADDR);

	/* get end address */
	pushBack = 0;
	end = start+1;
	_EP__PUTSTR("] to [");
	repChar('_',_EP_ADDR);
	put(']');
	repChar('\b',_EP_ADDR+1);
	result = getNumber(&end,_EP_ADDR);

	if(end < start){
		_EP__PUTSTR("<-- end address less than start address?" CRLF);
		return;
	}

	/* get fill byte */
	pushBack = 0;
	_EP__PUTSTR("] with [__]\b\b\b");
	result = getByte(&val);

	_EP__PUTSTR(CRLF);
	while(start <= end){
		*(uint8_t*)start = val;
		if((start & 0x01f) == 0) /* put out address every 32 */
			putAddr(start);put('\r'); /* use \r to stay on same line */
		start += 1;
	}
	_EP__PUTSTR(CRLF);
}
#endif

#ifndef NCHECKSUM
/*****************************************
checksum memory.
chksum [start] to [end]
 ****************************************/
void chksum()
{
	int16_t result = OK__;
	uintptr_t start = 0;
	uintptr_t end = 0x7ffff;
	uint32_t sum = 0;
	_EP__PUTSTR("\bhecksum [");

	/* get start address */
	pushBack = 0;
	repChar('_',_EP_ADDR);
	put(']');
	repChar('\b',_EP_ADDR+1);
	result = getNumber(&start,_EP_ADDR);

	/* get end address */
	pushBack = 0;
	//	end = start+1;
	_EP__PUTSTR("] to [");
	repChar('_',_EP_ADDR);
	put(']');
	repChar('\b',_EP_ADDR+1);
	result = getNumber(&end,_EP_ADDR);

	if(end < start){
		_EP__PUTSTR("<-- end address less than start address?" CRLF );
		return;
	}

	/* tell user to wait */
	pushBack = 0;
	_EP__PUTSTR("] wait...");

	_EP__PUTSTR(CRLF);
	while(start <= end){
	sum += *(uint8_t*)start;
	if((++start & 0x0ff) == 0) /* put out address every 256 */
		{
		putAddr(start);put('\r'); /* use \r to stay on same line */
		/* check if user wants to exit */
		if((rxChar()) != -1)
		start = end;
		}
	}
	_EP__PUTSTR(CRLF"Checksum: ");
	putHex(sum,8);
	_EP__PUTSTR(CRLF);
}
#endif

#ifndef NCOPY
/*****************************************
copy memory.
copy [size] bytes from [start] to [dest]
 ****************************************/
void copy()
{
	int16_t result = OK__;
	uintptr_t start = 0;
	uintptr_t dest = 0;
	uint32_t size = 0;
	uint32_t count = 0;

	_EP__PUTSTR("opy [");

	/* get size of block to copy  */
	pushBack = 0;
	repChar('_',_EP_ADDR);
	_EP__PUTSTR("] bytes");
	repChar('\b',_EP_ADDR+7);
	result = getNumber(&size,_EP_ADDR);

	_EP__PUTSTR("] bytes from [");
	/* get start address */
	pushBack = 0;
	repChar('_',_EP_ADDR);
	put(']');
	repChar('\b',_EP_ADDR+1);
	result = getNumber(&start,_EP_ADDR);

	/* get dest address */
	pushBack = 0;
	dest = start+size;  /* default to move just after the end location. */
	_EP__PUTSTR("] to [");
	repChar('_',_EP_ADDR);
	put(']');
	repChar('\b',_EP_ADDR+1);
	result = getNumber(&dest,_EP_ADDR);

	_EP__PUTSTR(CRLF);
	while(count != size){
		*(uint8_t*)dest = *(uint8_t*)start;
		if((start & 0x0f) == 0) /* put out address every 16 */
			putAddr(start);put('\r'); /* use \r to stay on same line */
		start += 1;
		dest += 1;
		size += 1;
	}
	_EP__PUTSTR(CRLF);
}
#endif


#ifndef NCALL
/***************************
call [address]
Default call address is set to quit() by debugMonitor().
Default call address is set to S7 record address.
Future: may want to save our stack pointer and
restore it after the call.

 ****************************/
void call()
{
	uint32_t (*callFnc)(...);
	uint32_t ret;

	pushBack = 0;
	_EP__PUTSTR("ll [");
	repChar('_',_EP_ADDR);
	put(']');
	repChar('\b',_EP_ADDR+1);
	getNumber(&callAddr,_EP_ADDR);
	callFnc = (uint32_t (*)(...)) callAddr;
	ret = (*callFnc)();
	_EP__PUTSTR(CRLF "result:");
	putHex( ret, 8 );
	_EP__PUTSTR(CRLF);

}
#endif
/************************************************************
*
* parseIntel
* parseSrec
*
* Parse motorola S2, S3 records or Intel hex
* in cmd[] and loads a binary image into srecdata[]
* returns 0 (OK__) if OK (or if S0 record; srecdata[0] = 0).
* returns 1 if length error
* returns 2 if checksum error
* returns 3 if not an S0, S2, or S3 record.
* returns 9 if S7, S8, or S9 callAddr set and srecdata[0] = 0.
*
* Format of srecdata[] on exit: (S3 record input)
* srecdata[0] has length of data in srecdata[3:n]
* srecdata[1] S-record type number (1, 2, or 3 for S1, S2, or S3 respectively)
* srecdata[2] undefined.  Used to keep address and data aligned on 4 byte boundary.
* srecdata[3] S-Record length field.
* srecdata[4] MSB of start address
* srecdata[5]
* srecdata[6]
* srecdata[7] LSB of start address
* srecdata[8] first byte of data.
* . . .
* srecdata[n] Checksum
*
* s0  is a hex encoded name for the s record set, echoed to output.
* S1  is a 2 byte address data record,
* S2  is a 3 byte address data record,
* s3  is a 4 byte address data record
* s7  is a 6 byte start address record - stored in callAddr
* s8  is a 5 byte start address record
* s9  is a 4 byte start address record
*
*************************************************************/
#ifndef NSREC
#define CMD_LEN (128)
char cmd[CMD_LEN];
uint8_t srecdata[CMD_LEN/2] __attribute__ ((aligned (8)));

uint32_t checkSum; /* download checksum */
uint16_t errorCnt; /* download error counter */

/*********************************************************************
 *
 * parseSrec()
 *
 * parse data in cmd[] as an S-Record. Store decoded binary in srecdata[]
 *
 * For S1 record:
 * srecdata[0] will have the number of bytes received.
 * srecdata[1] S-record type number (1, 2, or 3 for S1, S2, or S3 respectively)
 * srecdata[2:4] unused.
 * srecdata[5] will have s-record length field
 * srecdata[6-7] has destination 2 byte (16 bit) address (Big Endian)
 * srecdata[8] is the start of the data.
 *
 * For S2 record:
 * srecdata[0] will have the number of bytes received.
 * srecdata[1] S-record type number (1, 2, or 3 for S1, S2, or S3 respectively)
 * srecdata[2:3] unused.
 * srecdata[4] will have s-record length field
 * srecdata[5-7] has destination 3 byte (24 bit) address (Big Endian)
 * srecdata[8] is the start of the data.
 *
 * For S3 record:
 * srecdata[0] will have the number of bytes received.
 * srecdata[1] S-record type number (1, 2, or 3 for S1, S2, or S3 respectively)
 * srecdata[2] unused.
 * srecdata[3] will have s-record length field
 * srecdata[4-7] has destination 4 byte (32 bit) address (Big Endian)
 * srecdata[8] is the start of the data.
 *
 * return non zero if last record seen or error
 *  0 - if data was parse successfully
 *  1 - bad length element
 *  2 - bad checksum
 *	3 - if no S record found in cmd[]
 *  9 - start address S7/S8/S9 record received. callAddr set to it.
 *
 *********************************************************************/
int16_t parseSrec()
{
	int16_t i, ohead;	// offset to header start.
	int16_t num;
	int16_t sum;
	srecdata[ SREC_DATA_BYTES_I ] = 0;
	if (cmd[0] == 's' && (cmd[1] >= '0' && cmd[1] <= '3'))
	{
		/*
		 * Translate every 2 characters in cmd into 1 byte of data
		 * ohead is the the address, the checksum plus one byte.
		 */
		if( cmd[1] == '3' )
		{
			ohead = S3REC_OHEAD;		/* 5  S3 record */
			srecdata[SREC_TYPE_I]=3;	/* S3 record */
		}else if( cmd[1] == '2' )
		{
			ohead = S2REC_OHEAD; 		/* 4  S2 record */
			srecdata[SREC_TYPE_I]=2;	/* S2 record */
		}else /* S1 or S0 */
		{
			ohead = S1REC_OHEAD; 		/* 3  S1 record */
			srecdata[SREC_TYPE_I]=1;	/* S1 record */
		}
		/* parse s-record hex bytes and place in srecdata[] */
		for (i = 1; i < ((CMD_LEN/2)-8); i++)
		{
			num = get2(cmd + i + i);
			if (num < 0)
				break; // LF or CR indicates new line.
			srecdata[(i-1)+SREC_DATA_OFFSET-ohead] = num;
		}

		/*
		 * Check for valid record length
		 * at either data[3] or data[4] or data[5]
		 * for the S3, S2, or S1 record respectively (add 2 for the 'Sn' characters counted in i).
		 * number of data bytes should be Length-Field minus 5 for S3, minus 4 for S2, and minus 3 for S1
		 */
		if ((srecdata[SREC_DATA_OFFSET - ohead] + 2) != i)
		{
			_EP__PUTSTR(CRLF "Bad length: ");
			putHex(i, 2);
			_EP__PUTSTR(CRLF);
			return 1;
		}

		/*
		 * Check valid checksum in the record
		 */
		sum = 0;
		num = i - 1;
		for (i = SREC_DATA_OFFSET-ohead; i < num+SREC_DATA_OFFSET-ohead; i++)
			sum += srecdata[i];
		checkSum += sum;  // keep running checksum
		if ((sum & 0x0ff) != 0x0ff)
		{
			_EP__PUTSTR( CRLF "Bad checksum: ");
			putHex(sum, 2);
			_EP__PUTSTR(CRLF);
			return 2;
		}
		srecdata[SREC_DATA_BYTES_I] = srecdata[SREC_DATA_OFFSET-ohead] - (ohead);  /* store length of data (not including address field length and checksum field)*/

		if (cmd[0] == 's' && (cmd[1] == '0'))
		{
			errorCnt = 0;  // reset error count at start of load.
			checkSum = 0;  // reset checksum
			srecdata[SREC_DATA_OFFSET+srecdata[SREC_DATA_BYTES_I]]=0; // null terminate string.
			_EP__PUTSTR(" <");
			_EP__PUTSTR((char*) &srecdata[SREC_DATA_OFFSET]);
			_EP__PUTSTR(">" CRLF);
			srecdata[SREC_TYPE_I]=0;
			srecdata[0] = 0;
		}
		return OK__;

	}else if ( cmd[0] == 's' &&
			( cmd[1] == '7' || cmd[1] == '8' ||cmd[1] == '9'))
	{
		// start address given so put it as the default call address
		callAddr = 0;
		ohead = get2( &cmd[2]);	// length field
		ohead--;				// don't count checksum byte.
		for (i = 0; i < ohead ; i++)
		{
			num = get2( &cmd[4+i+i]);
			callAddr = callAddr << 8;
			callAddr |= num;
		}
		_EP__PUTSTR(CRLF "Default call address set to: ");
		putHex(callAddr, _EP_ADDR);
		srecdata[SREC_TYPE_I]= cmd[1] -'0';
		srecdata[SREC_DATA_BYTES_I] = 0;
		return 9;
	}else if ( cmd[0] == 's' &&
			( cmd[1] == '4' || cmd[1] == '5' ||cmd[1] == '6'))
	{
		// ignore these records.
		srecdata[SREC_TYPE_I]= cmd[1] -'0';
		srecdata[SREC_DATA_BYTES_I] = 0;
		return OK__;
	}
	return 3;	/* non-srec data */
}
/************************************************************
*
* download
* On entry cmd[] has first S-record
* Loops processing input lines until no valid S-Record found or
* start address S7/S8/S9 record received.
*
* Down load motorola S2, S3 records
*************************************************************/
#ifndef NSREC_DLD
void download()
{
	uintptr_t addr;
	int16_t i;
	put( XOFF );
	srecdata[0]=0;
	do{
		i = 9;
		if( cmd[0] == 's'  )
		{
			i=parseSrec();
		}
		if( i != OK__ )
		{
			if (i != 9) errorCnt++; // increase the errorCount
			_EP__PUTSTR(CRLF "Exit load. Error Count:");
			putHex(errorCnt, 4);
			_EP__PUTSTR(" Checksum:");
			putHex(checkSum,8);
			_EP__PUTSTR(CRLF);
			put(XON);
			return;
		}

		/*
		 * Decode the record address and store the record data in the
		 * specified location.
		 */
		addr = (srecdata[2] << 24) + (srecdata[3] << 16) + (srecdata[4] << 8) + srecdata[5];

		for (i = 0; i < srecdata[0]; i++)
		{
			*((uint8_t*)(addr + i)) = srecdata[i+SREC_DATA_OFFSET];
		}
		put( XON );
		gets__( cmd, CMD_LEN);
		put( XOFF );
	}while( cmd[0] == 's' || (cmd[0] == ':' && cmd[8] == '0'));

		put(XON);
	_EP__PUTSTR(CRLF);
}
#endif /*  */
#else
	/* no S-record support */
#define CMD_LEN (32)
char cmd[CMD_LEN];

#endif

#ifndef NUI
void quit()
{
	/* dummy function  to make call cmd not crash */
}

/***************************************
Command parser
 **************************************/
struct table{
	const char* cmd; 		/* command string */
	const char* help; 	/* short help string about command */
	void (*cmdFn)();		/* pointer to the command */
};
void help();
void about();
void unknown();

const table cmdTable[] = {
	{" ","Enter command character(s) followed by SPACE to enter parameters" CRLF
	" or followed by <CR> to use default parameters." CRLF ,quit},
	{"cmd ","\tDescription",quit},
#ifndef NABOUT
	{"a","\tabout this debugger", about},
#endif
#ifndef NCOPY
	{"c","\tcopy [size] bytes from [source] to [dest]",copy},
#endif
#ifndef NCALL
	{"ca","\tcall [address]",call},
#endif
#ifndef NCHECKSUM
	{"cs","\tchecksum [start] to [end]",chksum},
#endif
#ifndef NDUMP
	{"d","\tdump [start] to [end]",dump},
#endif
#ifndef NFILL
	{"f","\tfill [start] to [end] with [byte]",fill},
#endif
#ifndef NMEMBYTE
	{"m", "\tbyte modify [start], CR to exit, SP to advance, BS to go back",
	modifyMemByte},
#endif
#ifndef NMEMWORD
	{"wm","\tword modify [start], ...",modifyMemWord},
#endif
#ifndef NMEMLONG
	{"lm","\tlong modify [start], ...",modifyMemLong},
#endif
	{"h\007elp","\thelp, displays this screen",help},
	{"?","\thelp, displays this screen",help},
	{"q\007uit","\tquit debug monitor",quit},
#ifndef NRESET
	{"reset","\treset system",reset},
#endif
#ifndef NSREC
	{"s\007","\ts1, s2 or s3 records for downloading", download},
#endif
#ifdef EP_CONFIG_USE_PLATFORM_ADDITIONS
	EP_CMD_TBL_PLATFORM_ADDITIONS
#endif
	/* next two entries just for help info */
	{"<CR>", "\trepeat last command with default parameters",unknown},
	{"SPACE","\trepeat last command, prompt for new parameters",unknown},
	{"","",unknown} /* must be last entry in command table */
};
/*
help()

outputs the above command table for help info
 */
void help()
{
	int16_t i = 0;
	_EP__PUTSTR(CRLF CRLF SIGN_ON CRLF);
	while(cmdTable[i].cmd[0] != 0){
		_EP__PUTSTR(CRLF);
		_EP__PUTSTR(cmdTable[i].cmd);
		_EP__PUTSTR(cmdTable[i++].help);
	}
	_EP__PUTSTR(CRLF);
}
/*
about()

Outputs the screen telling about this debugger.
Total self indulgence by the author of this.
 */
#ifndef NABOUT
void about()
{
	_EP__PUTSTR(CRLF CRLF SIGN_ON CRLF);
	_EP__PUTSTR(aboutStr);
}
#endif

void unknown()
{
	_EP__PUTSTR("<--Unknown command" CRLF);
}
char getCmd(char* str)
{
	char c;
	while((c = get()) > 0x21){ /* loop until !, space, LF or CR */
		*str++ = c | 0x20; /* make all lower case */
		put(c); 			/* echo character */
	}
	*str = 0;
	pushBack = c;	/* push back the last char that caused return */
	return c;	/* return the character that terminated input */
}
/*************************************************************

debugMonitor()

This is the entry point for the debug monitor.
This will return only if the (q)uit command is given.

**************************************************************/
void debugMonitor(void)
{
	char c;
	char last = ' ';
	int16_t i;
	void (*lastCmd)(); /* pointer to the last command executed */

	/* initialize global vars */
	pushBack = 0;
	memAddr = 0;
	dmpAddr = 0;
	callAddr = (uintptr_t) quit; /* set a safe default call address */
	lastCmd = unknown;

	_EP__PUTSTR(CRLF SIGN_ON CRLF);
	for(;;){	/* loop forever processing commands as they come in */
		i = 0;
		cmd[0] = 0;
		pushBack = 0;
#ifndef NSWHNDSHK
		_EP__PUTSTR(STRINGIFY(XOFF)"cmd: "STRINGIFY(XON)); /* bracket with XOFF/XON to help downloads*/
#else
		_EP__PUTSTR("cmd: "); /* */
#endif
		c = getCmd(cmd);	/* get command string */
		if(cmd[0] == 0){	/* check if repeat last command */
			pushBack = c;
			put(last);
		}
		else if( cmd[0] == 's')
		{ //check for S here to speed up s-record processing.
			lastCmd = download;
		}else if( cmd[0] == '#')
		{
		gets__( cmd, CMD_LEN);
		_EP__PUTSTR(CRLF);
		continue; // # is a comment line. ignore it.
		}else
		{ /* check if known command */

			while(cmdTable[i].cmd[0] != 0
				&& strcmp__(cmdTable[i].cmd, cmd))
				i++;
			lastCmd = cmdTable[i].cmdFn;
			last = cmdTable[i].cmd[0];
		}
		if(lastCmd == quit){
			_EP__PUTSTR("uit" CRLF);
			return;
		}
		(*lastCmd)();	/* execute the command */
	} /* end of for loop */
}
#endif /* end of ifndef NUI */



/* ------  Version history  -----------------------------------------------
0.1  1979 assembly version for my 8080 computer I built in high school.
1.0  1987 First version in C of the assembly language one.
1.1  nov-1996 Added S record download support.
1.2  oct-1998 Added Flash programming support
	improved speed problem in S-record
	Added Intel hex support.
	mar-1999 Added the copy command.
1.2.1 Jan-2001 Removed Flash support and intel hex support to make it smaller.
	Pulled in baud rate setting feature from my version 1.3.9 68K debug monitor.
1.2.3 2003 Added nvram support for Diseqc H-H Motor project (my home-brew satelite TV dish antenna positioner)
1.3.0 2017-2018 Made more portable and configurable for further use in some new projects.
1.3.1 2024 Fixed bug with uintptr_t use. Cleaned up some formatting of strings. Added no local echo option.
	Changed to using LF as EOL instead of CR, made EOL configurable.
	---------------------------------------------------------------------- */

