#ifndef EPPLTADDH
#define EPPLTADDH
/** @file eppltadd.h
This file is for configuring platform specific additions.
*/
/**
Copyright 1979-2024 Mark Giebler

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Author: Mark Giebler

 */
#ifndef EPDBCONFIGHXX
	#error "include eppltadd.h must appear in source file AFTER include epdbcnfg.h"
#endif

#ifdef __cplusplus
extern "C" {	/* to compile with a C++ compiler */
#endif

/* Define to include platform specific function and command additions */

/* enable / disable specific additions */
#define NBAUD 1
#define NPFLASH 1

#ifndef NUI

/* Define additional platform command table elements */
#define EP_CMD_TBL_PLATFORM_ADDITIONS  										\
#ifndef NBAUD																\
	{"b","\tbaud [48|96|19|38|57] bits [7|8] parity [e|o|n]",baud},			\
#endif																		\
#ifndef NPFLASH																\
	{"p","\tprogram Flash", pflash},										\
#endif																		\
/* end of cmd table additions */

#endif  /* end of ifndef NUI */


#ifdef __cplusplus
}
#endif

#endif
