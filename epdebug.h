/** @file epdebug.h
Copyright 1979-2024 Mark Giebler

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Author: Mark Giebler

 */
/*
Configure debug features in epdbcnfg.h
*/
#include "epdbcnfg.h"
/*
Put platform specific character IO prototypes/macros in eppltfrm.h
*/
#include "eppltfrm.h"


#ifndef EPDEBUGHXX
#define EPDEBUGHXX


#ifdef __cplusplus
extern "C" {	/* to compile with a C++ compiler */
#endif

/* ***********************************************
*  Global variables needed for public functions
**************************************************/
extern uintptr_t dmpAddr ;	/* dump start address */

typedef enum{	// alignment of dump hex digits to the heading line
	absoluteAlignment,
	relativeAlignment
}ep_dumpMode_t;

/* ***********************************************
* Public functions
**************************************************/
void debugInit(void);
void debugMonitor(void);
void put(char c);
#ifdef _EP__PUTSTR
//void putStr () __attribute__ ((weak, alias ("debug_puts")));
#define putStr(s) _EP__PUTSTR(s)
#else
void putStr( const char* str );
#endif
void putByte(uint8_t byte);
void putWord(uint16_t word);
void putAddr(uintptr_t addr);

uintptr_t dmpLine(uintptr_t start, uintptr_t end, ep_dumpMode_t dmode);
uintptr_t dumpMemory(uintptr_t start, uintptr_t end, ep_dumpMode_t dmode);

/* ***********************************************
 * variables external routines may need access to.
 *************************************************/
extern uintptr_t dmpAddr;	/* dump address */
extern uintptr_t memAddr;	/* modify memory address */
extern uintptr_t callAddr;	/* call addr */

/* ***********************************************
* S-Record parser and ihex parser related
* ************************************************/
extern char cmd[];			/* raw srec input string to parser */
int16_t parseSrec();
extern uint8_t srecdata[];	/* output bytes of the srec parser */
extern uint16_t errorCnt; 	/* download error counter */
/* index into srecdata[] for data bytes converted from S-record input (not including length field or address field)*/
#define SREC_DATA_BYTES_I	(0)
/* index for s-record type number (1, 2, or 3 for S1, S2, or S3 respectively) */
#define SREC_TYPE_I			(1)
/* srecdata[] index to s-record length field: S1 */
#define S1REC_LENGTH_I		(5)
/* srecdata[] index to s-record length field: S3 */
#define S2REC_LENGTH_I		(4)
/* srecdata[] index to s-record length field: S3 */
#define S3REC_LENGTH_I		(3)
/* srecdata[] index to s-record address field: S1 */
#define S1REC_ADDRESS_I		(S1REC_LENGTH_I+1)
/* srecdata[] index to s-record address field: S3 */
#define S2REC_ADDRESS_I		(S2REC_LENGTH_I+1)
/* srecdata[] index to s-record address field: S3 */
#define S3REC_ADDRESS_I		(S3REC_LENGTH_I+1)
/* srecdata[] offset to the start of s-record payload data */
#define SREC_DATA_OFFSET	(8)
/* offset for binary work. offset backwards from SREC_DATA_OFFSET at which to place length field binary value, followed by address, */
#define S1REC_OHEAD			(3)
#define S2REC_OHEAD			(S1REC_OHEAD+1)
#define S3REC_OHEAD			(S2REC_OHEAD+1)

#ifdef __cplusplus
}
#endif

#endif /*  EPDEBUGHXX */
