/** @file
 * mgDebugHelper.h
 *
 *  Author: Mark Giebler
 *  Created on: 2002-02-24  for my H-H satellite tracking motor project
 *
 *  Define macros to make it easy to remove blocks of debug code
 *  and to comment out blocks of code easily with out getting
 *  tripped up by comment delimiters in the code block.
 */

#ifndef MGDEBUGHELPER_H_
#define MGDEBUGHELPER_H_

/// -----------------------------------------------------
/// Mark's debug macro helpers
/// -----------------------------------------------------
/// @brief G_DEBUG() use enclose debug only code that can be disabled by redefining.
// 		_mg_: put any debug only code inside the parenthesis (represented by 'code') in below macro.
//  	'code' can be multiple statements on multiple lines.
// 		comment out next line to turn off all debug code.
//
#define G_DEBUG(code)  {code;}
#ifndef G_DEBUG
// 		if G_DEBUG() is commented out above, then define NULL debug macro that throws 'code' away.
#define G_DEBUG(code) {;}
#define G_DEBUG_LEVEL_SETTING	0	/* disabled debug level ** DO NOT CHANGE THIS ** */
#else
#define G_DEBUG_ENABLED
#ifndef G_DEBUG_LEVEL_SETTING
#define G_DEBUG_LEVEL_SETTING	3	/* set higher to enable more verbose debug output ** Change this to set verbose level ** */
#endif
#endif

//! @brief 		use G_DEBUG_LEVEL(LEVEL,code) to enable code only if debug LEVEL <= G_DEBUG_LEVEL_SETTING
#define G_DEBUG_LEVEL(lvl,code) if(G_DEBUG_LEVEL_SETTING>=lvl)G_DEBUG(code)

//! @brief 		use G_DEBUG_LEVEL_N() macro to temporarily disable an individual G_DEBUG_LEVEL_N()  block of code
#define G_DEBUG_LEVEL_N(lvl,code) {;}

//! @brief 		use G_DEBUG_IFTRUE(testcon,code) to enable code only if testcon is true
#define G_DEBUG_IFTRUE(testcon,code) if(testcon)G_DEBUG(code)

//! @brief 		use G_DEBUG_IFTRUE_N() macro to temporarily disable an individual G_DEBUG_IFTRUE_N()  block of code
#define G_DEBUG_IFTRUE_N(testcon,code) {;}

//! @brief 		use G_DEBUG_N() macro to temporarily disable an individual G_DEBUG()  block of code
#define G_DEBUG_N(code) {;}

//! @brief 		use G_DEBUG_ISR() special one for ISR debug blocks
#define G_DEBUG_ISR(code) G_DEBUG(code)
// 				use G_DEBUG_ISR_N()  for removing specific ISR debug blocks
#define G_DEBUG_ISR_N(code) G_DEBUG_N(code)

//! @brief 		use G_COMMENT_OUT() for commenting out blocks of code with embedded comments.
#define G_COMMENT_OUT(...)	{;};
//! @brief 		use G_COMMENT_OUT_N() to temporarily re-enable a block of code that was commented out by above macro
#define G_COMMENT_OUT_N(code_args...) code_args

// Define some macros to turn text into quoted strings:
// two layers of indirection required to not get __LINE__ turned into "__LINE__"
// e.g. G_STRINGIFY(Foo) -->  "Foo"   and   G_STRINGIFY(__LINE__) --> "62"
#ifndef _STRINGIFY
#define _STRINGIFY(name)  #name		/* helper macro, do not use directly */
#endif
#ifndef G_STRINGIFY
#define G_STRINGIFY(a) _STRINGIFY(a)
#endif
// -----------------------------------------------------
// in line functions
// -----------------------------------------------------
#ifdef G_DEBUG_ENABLED
/*! @brief G_DUMP() outputs via debug IO a hex dump of a block of memory */
/* uintptr_t G_DUMP(...)  */
#define G_DUMP(title, memPtr, size) \
	({uintptr_t retVal;  putStr( title ); \
	retVal = dumpMemory((uintptr_t)memPtr, (uintptr_t)memPtr + (uintptr_t)(size), (ep_dumpMode_t)relativeAlignment ); \
	retVal; }) /* returns the address from dumpMemory() */
#else
#define G_DUMP(title, memPtr, size)
#endif

// -----------------------------------------------------
// END -- Mark's debug macro helpers
// -----------------------------------------------------

#endif /* MGDEBUGHELPER_H_ */
