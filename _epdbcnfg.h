#ifndef EPDBCONFIGHXX
#define EPDBCONFIGHXX
/** @file epdbcnfg.h
This file is for configuring the epdebug.c features.
*/
/*
Copyright 1979-2024 Mark Giebler

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Author: Mark Giebler

 */
#ifdef EPPLATFORMHXX
#error "include eppltfrm.h" must appear in source files AFTER "include epdbcnfg.h"
#endif

#ifdef EPDEBUGHXX
#error "include epdebug.h" must appear in source files AFTER "include epdbcnfg.h"
#endif


#define NUI 1		/* define if the user interface command  menu system is not needed */
//#define NINPUT 1	/* define if no input methods are desired (i.e. output only methods needed) */
#define NECHO 1		/* define is no local echo on input chars */

#define NCALL 1 	/* define if call function not desired */
#define NCHECKSUM 1 /* define if checksum function not desired */
#define NCOPY 1   	/* define if we don't want copy command, to make code smaller */
//#define NDUMP 1 	/* define if no dump command is desired */
//#define NFANCYHDR 1	/* define if no fancy header (just simple header) for dump command */
#define NFILL 1   	/* define if we don't want fill command, to make code smaller */
#define NMEMBYTE 1  /* define if we don't want m command, to make code smaller */
#define NMEMWORD 1  /* define if we don't want mw command, to make code smaller */
#define NMEMLONG 1  /* define if we don't want ml command, to make code smaller*/


//#define NABOUT 1	/* define if no long about string */
#define NINTEL 1 	/* define if no intel downloading */
#define NSREC 1		/* define if no S-Record parser desired */
#define NSREC_DLD 1	/* define if no SREC download handler is needed */
#define NSWHNDSHK 1	/* define if no software xon/xoff handshake */
#define NRESET 1	/* define if reset function not desired */

/* Define to include platform specific function and command additions in eppltadd.c and eppltadd.h */
#define EP_CONFIG_USE_PLATFORM_ADDITIONS

//  ---  Configure platform newline ----
//#define CRLF "\r\012"		/* For M$ Windows fans */
#define CRLF "\n"			/* newline sequence */
#define ABORT_CHAR	'\n'	/* newline character that will abort a long residence function early. */
#define EOL_IGNORE	'\r'	/* Ignore CR - most likely from a M$ Windows system. Or if you like wasting chars, set to LF for Windows fans. */

#endif /* EPDBCONFIGHXX */
