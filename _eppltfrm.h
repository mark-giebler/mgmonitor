#ifndef EPPLATFORMHXX
#define EPPLATFORMHXX
/** @file eppltfrm.h
	Put platform specific character IO prototypes and macros in this file.
*/
/**
Copyright 1979-2024 Mark Giebler

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Author: Mark Giebler

*/
#ifndef EPDBCONFIGHXX
#error "include eppltfrm.h" must appear in source files AFTER "include epdbcnfg.h"
#endif
#ifdef EPDEBUGHXX
#error "include eppltfrm.h" must appear in source files BEFORE "include epdebug.h"
#endif


#ifdef __cplusplus
extern "C" {	/* to compile with a C++ compiler */
#endif

/* *************************************************
* 		Platform specific typedefs
****************************************************/
#include <stdint.h>	/* get the C standard typedefs */

/* if platform does not have a stdint.h   and/or
does not have these types, define them:
int8_t
int16_t
int32_t
uint8_t
uint16_t
uint32_t
uintptr_t
*/

/*
typedef  char int8_t;
typedef unsigned char uint8_t;
typedef  short int16_t;
typedef unsigned short uint16_t;
typedef  long int32_t;
typedef unsigned long uint32_t;
typedef uintptr_t uint32_t*
*/

/* define integer type that can hold a uintptr_t, used for inputing an address */
/* on 16 bit systems this would be defined as uint16_t, on 32 bit system uint32_t */
typedef uint32_t uintptr_num_t;
_Static_assert ( sizeof(uintptr_num_t) == sizeof(uintptr_t) , "Address size mismatch for this platform between uintptr_num_t  and  uintptr_t");

#define _EP_ADDR (8)	/* target CPU's max number of hex digits per address value that can be entered. Typically 4 or 8 */
_Static_assert ( sizeof(uintptr_num_t) == (_EP_ADDR/2) , "Address size mismatch for this platform between uintptr_num_t  and  _EP_ADDR");



/* data representation in memory */
//#define _EP_LITTLE_ENDIAN  /* define if target is little endian -  NOT CURRENTLY IMPLEMENTED - */

/* ************************************************
* macros for IO routines that need to be
* defined for the platform.
* Some are optional
************************************************** */
/* include any header files for serial IO functions here */

/* *********************************************** */

/* _EP__INIT()	 OPTIONAL - declare any IO initialization that need to be done for the platform */
#define _EP__INIT()

/* _EP__PUT(c) put an 8 bit (int8_t) character to the serial IO device */
#define _EP__PUT(c)		_FOO___.put(c)

/* _EP__PUTSTR(s) OPTIONAL - put a null terminated string to the serial IO device */
/*  - if not defined, a built in function will be used. */
//#define _EP__PUTSTR(c)

/* _EP__GETCH()  non-blocking:  return a character from the serial IO device, return -1 if no char (return a 16 bit int)*/
#define _EP__GETCH()	(-1)	/* for now return -1  */

/* _EP__GET()  OPTIONAL - blocking: get character call */
/*  - if not defined, a built in function will be used. */
#define _EP__GET()		_FOO___.get()

#ifndef NSWHNDSHK
/* OPTIONAL  only required for checking for XOFF  - if not defined, a built in function will be used. */
//#define _EP__RX_WAITING() _FOO___.rxWaiting() /* return same as _EP__GETCH() but does not remove char from queue */
#endif

/* OPTIONAL  reset the CPU  - does not return */
#define _EP__MCU_RESET()  _FOO___.reset()

/* *********************************************** */
#ifdef __cplusplus
}
#endif
/* *********************************************** */

#endif /* EPPLATFORMHXX */

